package gusl.auth.repository.elastic;

import gusl.auth.model.session.SessionDO;
import gusl.auth.model.session.SessionStatus;
import gusl.auth.repository.SessionDAO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.elastic.dao.AbstractDAOElasticWithConfirmationImpl;
import gusl.elastic.model.ElasticIndex;
import gusl.elastic.utils.ElasticConstants;
import gusl.query.MatchQuery;
import gusl.query.QueryParams;
import org.jvnet.hk2.annotations.Service;

import java.util.List;

@Service
@ElasticIndex(index = SessionIndex.INDEX, version = SessionDO.VERSION, type = SessionDO.class)
public class SessionIndex extends AbstractDAOElasticWithConfirmationImpl<SessionDO> implements SessionDAO {
    public static final String INDEX = ElasticConstants.ELASTIC_INDEX_PREFIX + "session";

    public SessionIndex() {
        super(SessionDO.class);
    }

    @Override
    public List<SessionDO> getAllForUser(String userId) throws GUSLErrorException {
        return get(QueryParams.builder()
                .must(MatchQuery.of("userId", userId))
                .build());
    }

    @Override
    public SessionDO updateStatus(String id, SessionStatus newStatus) throws GUSLErrorException {
        SessionDO session = findRecord(id);
        session.setStatus(newStatus);
        return update(session);
    }

    @Override
    public String getIndexName() {
        return INDEX;
    }
}
