package gusl.auth.repository.elastic;

import gusl.auth.model.geofence.GeoFenceDO;
import gusl.auth.repository.GeoFenceDAO;
import gusl.elastic.dao.AbstractDAOElasticImpl;
import gusl.elastic.model.ElasticIndex;
import gusl.elastic.utils.ElasticConstants;
import org.jvnet.hk2.annotations.Service;

@Service
@ElasticIndex(index = GeoFenceIndex.INDEX, version = GeoFenceDO.VERSION, staticContent = "geo_fence.json", type = GeoFenceDO.class)
public class GeoFenceIndex extends AbstractDAOElasticImpl<GeoFenceDO> implements GeoFenceDAO {
    public static final String INDEX = ElasticConstants.ELASTIC_INDEX_PREFIX + "geo_fence";

    public GeoFenceIndex() {
        super(GeoFenceDO.class);
    }

    @Override
    public String getIndexName() {
        return INDEX;
    }
}
