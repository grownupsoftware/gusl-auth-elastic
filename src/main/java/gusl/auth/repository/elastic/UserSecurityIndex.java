package gusl.auth.repository.elastic;

import gusl.auth.model.security.UserSecurityDO;
import gusl.auth.repository.UserSecurityDAO;
import gusl.elastic.dao.AbstractDAOElasticImpl;
import gusl.elastic.dao.AbstractDAOElasticWithConfirmationImpl;
import gusl.elastic.model.ElasticIndex;
import gusl.elastic.utils.ElasticConstants;
import org.jvnet.hk2.annotations.Service;

@Service
@ElasticIndex(index = UserSecurityIndex.INDEX, version = UserSecurityDO.VERSION, staticContent = "user_security.csv", type = UserSecurityDO.class)
public class UserSecurityIndex extends AbstractDAOElasticWithConfirmationImpl<UserSecurityDO> implements UserSecurityDAO {
    public static final String INDEX = ElasticConstants.ELASTIC_INDEX_PREFIX + "user_security";

    public UserSecurityIndex() {
        super(UserSecurityDO.class);
    }

    @Override
    public String getIndexName() {
        return INDEX;
    }
}
