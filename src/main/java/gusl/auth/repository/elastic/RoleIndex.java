package gusl.auth.repository.elastic;

import gusl.auth.model.roles.RoleDO;
import gusl.auth.repository.RoleDAO;
import gusl.elastic.dao.AbstractDAOElasticWithConfirmationImpl;
import gusl.elastic.model.ElasticIndex;
import gusl.elastic.utils.ElasticConstants;
import org.jvnet.hk2.annotations.Service;

@Service
@ElasticIndex(index = RoleIndex.INDEX, version = RoleDO.VERSION, staticContent = "role.csv", type = RoleDO.class)
public class RoleIndex extends AbstractDAOElasticWithConfirmationImpl<RoleDO> implements RoleDAO {
    public static final String INDEX = ElasticConstants.ELASTIC_INDEX_PREFIX + "role";

    public RoleIndex() {
        super(RoleDO.class);
    }

    @Override
    public String getIndexName() {
        return INDEX;
    }
}
